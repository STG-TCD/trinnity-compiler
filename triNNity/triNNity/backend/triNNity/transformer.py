import math
import numpy as np
import operator
from functools import reduce

from triNNity.util.errors import CompilerError, print_stderr
from triNNity.frontend.graph import IRGraphBuilder, IRNodeMapper
from triNNity.frontend.layers import LayerKind
from triNNity.util.transformers import (DataInjector, DataReshaper, NodeRenamer, ReLUFuser, BatchNormScaleBiasFuser, BatchNormPreprocessor, ParameterNamer, ConcatTreeSplitter)

def prod(iterable):
    return reduce(operator.mul, iterable, 1)

class TrinnityNode(object):
    '''An intermediate representation for Trinnity operations.'''

    def __init__(self, op, **kwargs):
        # A string corresponding to the Trinnity operation
        self.op = op
        self.orig_op = op
        # Constructed arguments for the emitted code
        self.args = ""
        # Keyword arguments for the operation
        self.kwargs = kwargs
        # The source Caffe node
        self.node = None
        # The name/decl of the input buffer
        self.input_buffer = None
        self.input_buffers = None
        self.input_buffer_name = None
        # The name/decl of the weights buffer
        self.weights_buffer = None
        self.weights_buffer_name = None
        # The name/decl of the bias buffer
        self.bias_buffer = None
        self.bias_buffer_name = None
        # The name/decl of the mean buffer
        self.mean_buffer = None
        self.mean_buffer_name = None
        # The name/decl of the offset buffer
        self.offset_buffer = None
        self.offset_buffer_name = None
        # The name/decl of the scale buffer
        self.scale_buffer = None
        self.scale_buffer_name = None
        # The name/decl of the variance buffer
        self.variance_buffer = None
        self.variance_buffer_name = None
        # The name/decl of the output buffer (only if not an in-place layer)
        self.output_buffer = None
        self.output_buffer_name = None
        # Caffe has some layers that we need to process but basically ignore
        self.magic_layers = ['data', 'label']

    def format(self, arg):
        '''Returns a string representation for the given value.'''
        return "%s" % str(arg)

    def pair(self, key, value):
        '''Returns key=formatted(value).'''
        return '%s=%s' % (key, self.format(value))

    def emit(self):
        '''Emits the Python source for this node.'''

        has_relu = 'relu' in self.kwargs and self.kwargs['relu']
        has_group = 'group' in self.kwargs and self.kwargs['group']
        has_bias = 'biased' in self.kwargs and self.kwargs['biased']

        # Collect buffer declarations
        # Track which declared buffers are read from disk, and which are not
        read_decls = []
        plain_decls = []

        if (len(self.node.parents) == 1 and self.node.get_only_parent().name == 'data'):
            self.input_buffer_name = 'data'

        template_args = self.kwargs

        # Select the triNNity primitive corresponding to this op
        if (self.op == 'conv'):
            self.op = 'triNNity::generic::layer::GenericFusedConvolutionalLayer'
            act = 'triNNity::ACTIVATION_NONE'
            if has_relu:
              act = 'triNNity::ACTIVATION_RELU'

            # Set up input buffer
            if (self.op not in self.magic_layers):
                if (self.input_buffer_name is None):
                    papa = self.node.get_only_parent()
                    self.input_buffer_name = papa.name
                    self.input_buffer = ''
            else:
                self.input_buffer_name = self.node.name + '_input'
                self.input_buffer = 'A' + ' * ' + self.input_buffer_name + ';'
                read_decls += [(self.input_buffer_name, self.input_buffer, 'A', str(prod([template_args["c_i"], template_args["w_i"], template_args["h_i"]])))]

            # Set up weights buffer
            self.weights_buffer_name = self.node.name + '_weights'
            self.weights_buffer = 'W' + ' * ' + self.weights_buffer_name + ';'
            read_decls += [(self.weights_buffer_name, self.weights_buffer, 'W', str(prod([template_args["c_i"], template_args["k_w"], template_args["k_h"], template_args["c_o"]])))]

            # Set up bias buffer
            if has_bias:
                self.bias_buffer_name = self.node.name + '_biases'
                self.bias_buffer = 'W' + ' * ' + self.bias_buffer_name + ';'
                read_decls += [(self.bias_buffer_name, self.bias_buffer, 'W', str(template_args["c_o"]))]

            # Set up output buffer
            # self.output_buffer_name = self.node.name + '_output'
            # self.output_buffer = 'A' + ' * ' + self.output_buffer_name + ';'
            # plain_decls += [(self.output_buffer_name, self.output_buffer, 'A', str(prod([template_args["c_o"], template_args["w_o"], template_args["h_o"]])))]

            # Check if padding is required
            padding = None
            discard = False
            if self.kwargs['pad'] == 'discard':
                padding = 'triNNity::BOUND_IMPLICIT_PAD'
                discard = True
            elif self.kwargs['pad'] == 'implicit':
                padding = 'triNNity::BOUND_IMPLICIT_PAD'
            else:
                raise CompilerError("Unknown padding type requested: {}".format(self.kwargs['pad']))

            if discard:
                args = ', '.join(['A', 'W', 'A', 'LAYER_'+self.node.name.upper()+'_METHOD', 'GEMM_TYPE'] + list(map(str, [template_args["c_i"], template_args["w_i"], template_args["h_i"], template_args["k_w"], template_args["k_h"], template_args["s_w"], template_args["s_h"], template_args["c_o"]])) + ['LAYER_'+(self.node.name.upper())+'_IN_FMT', padding, 'triNNity::DISCARD', act])
            else:
                args = ', '.join(['A', 'W', 'A', 'LAYER_'+self.node.name.upper()+'_METHOD', 'GEMM_TYPE'] + list(map(str, [template_args["c_i"], template_args["w_i"], template_args["h_i"], template_args["k_w"], template_args["k_h"], template_args["s_w"], template_args["s_h"], template_args["c_o"]])) + ['LAYER_'+(self.node.name.upper())+'_IN_FMT', padding, 'triNNity::PRESERVE', act])

        elif (self.op == 'relu'):
            self.op = 'triNNity::layer::ActivationLayer'

            # Set up input buffer
            if (self.op not in self.magic_layers):
                if (self.input_buffer_name is None):
                    papa = self.node.get_only_parent()
                    self.input_buffer_name = papa.name
                    self.input_buffer = ''
            else:
                self.input_buffer_name = self.node.name + '_input'
                self.input_buffer = 'A' + ' * ' + self.input_buffer_name + ';'
                plain_decls += [(self.input_buffer_name, self.input_buffer, 'A', str(prod([template_args["c_i"], template_args["w_i"], template_args["h_i"]])))]

            args = ', '.join(['A'] + list(map(str, [template_args["c_i"], template_args["w_i"], template_args["h_i"]])) + [template_args["act"]])

        elif (self.op == 'max_pool'):
            self.op = 'triNNity::layer::PoolingLayer'

            # Set up input buffer
            if (self.op not in self.magic_layers):
                if (self.input_buffer_name is None):
                    papa = self.node.get_only_parent()
                    self.input_buffer_name = papa.name
                    self.input_buffer = ''
            else:
                self.input_buffer_name = self.node.name + '_input'
                self.input_buffer = 'A' + ' * ' + self.input_buffer_name + ';'
                plain_decls += [(self.input_buffer_name, self.input_buffer, 'A', str(prod([template_args["c_i"], template_args["w_i"], template_args["h_i"]])))]

            args = ', '.join(['A', 'triNNity::WINDOW_MAXPOOL'] + list(map(str, [template_args["c_i"], template_args["w_i"], template_args["h_i"], template_args["k_w"], template_args["k_h"], template_args["s_w"], template_args["s_h"], template_args["c_o"], template_args["w_o"], template_args["h_o"]])))

        elif (self.op == 'avg_pool'):
            self.op = 'triNNity::layer::PoolingLayer'

            # Set up input buffer
            if (self.op not in self.magic_layers):
                if (self.input_buffer_name is None):
                    papa = self.node.get_only_parent()
                    self.input_buffer_name = papa.name
                    self.input_buffer = ''
            else:
                self.input_buffer_name = self.node.name + '_input'
                self.input_buffer = 'A' + ' * ' + self.input_buffer_name + ';'
                plain_decls += [(self.input_buffer_name, self.input_buffer, 'A', str(prod([template_args["c_i"], template_args["w_i"], template_args["h_i"]])))]

            args = ', '.join(['A', 'triNNity::WINDOW_AVGPOOL'] + list(map(str, [template_args["c_i"], template_args["w_i"], template_args["h_i"], template_args["k_w"], template_args["k_h"], template_args["s_w"], template_args["s_h"], template_args["c_o"], template_args["w_o"], template_args["h_o"]])))

        elif (self.op == 'fc'):
            self.op = 'triNNity::layer::FCLayer'

            # Set up input buffer
            if (self.op not in self.magic_layers):
                if (self.input_buffer_name is None):
                    papa = self.node.get_only_parent()
                    self.input_buffer_name = papa.name
                    self.input_buffer = ''
            else:
                self.input_buffer_name = self.node.name + '_input'
                self.input_buffer = 'A' + ' * ' + self.input_buffer_name + ';'
                plain_decls += [(self.input_buffer_name, self.input_buffer, 'A', str(prod([template_args["c_i"], template_args["w_i"], template_args["h_i"]])))]

            # Set up weights buffer
            self.weights_buffer_name = self.node.name + '_weights'
            self.weights_buffer = 'W' + ' * ' + self.weights_buffer_name + ';'
            read_decls += [(self.weights_buffer_name, self.weights_buffer, 'W', str(prod([template_args["c_i"], template_args["w_i"], template_args["h_i"], template_args["c_o"]])))]

            # Set up bias buffer
            if self.kwargs['biased']:
                self.bias_buffer_name = self.node.name + '_biases'
                self.bias_buffer = 'W' + ' * ' + self.bias_buffer_name + ';'
                read_decls += [(self.bias_buffer_name, self.bias_buffer, 'W', str(template_args["c_o"]))]

            args = ', '.join(['A', 'W', 'GEMV_TYPE'] + list(map(str, [template_args["c_i"], template_args["w_i"], template_args["h_i"], template_args["c_o"]])))

        elif (self.op == 'softmax'):
            self.op = 'triNNity::layer::SoftmaxLayer'

            # Set up input buffer
            if (self.op not in self.magic_layers):
                if (self.input_buffer_name is None):
                    papa = self.node.get_only_parent()
                    self.input_buffer_name = papa.name
                    self.input_buffer = ''
            else:
                self.input_buffer_name = self.node.name + '_input'
                self.input_buffer = 'A' + ' * ' + self.input_buffer_name + ';'
                plain_decls += [(self.input_buffer_name, self.input_buffer, 'A', str(prod([template_args["c_i"], template_args["w_i"], template_args["h_i"]])))]

            args = ', '.join(['A'] + list(map(str, [template_args["c_i"]])))

        elif (self.op == 'lrn'):
            self.op = 'triNNity::layer::ChannelwiseLRNLayer'

            # Set up input buffer
            if (self.op not in self.magic_layers):
                if (self.input_buffer_name is None):
                    papa = self.node.get_only_parent()
                    self.input_buffer_name = papa.name
                    self.input_buffer = ''
            else:
                self.input_buffer_name = self.node.name + '_input'
                self.input_buffer = 'A' + ' * ' + self.input_buffer_name + ';'
                plain_decls += [(self.input_buffer_name, self.input_buffer, 'A', str(prod([template_args["c_i"], template_args["w_i"], template_args["h_i"]])))]

            args = ', '.join(['A'] + list(map(str, [template_args["c_i"], template_args["w_i"], template_args["h_i"], template_args["layout"]])))

        elif (self.op == 'concat'):
            self.op = 'triNNity::layer::ChannelwiseConcatLayer'

            # Set up input buffer
            self.input_buffers = []
            parents = self.node.get_all_parents()
            for n in parents:
              self.input_buffers.append(n.name)

            args = ', '.join(['A'] + list(map(str, [template_args["c_i_0"], template_args["c_i_1"], template_args["w_i"], template_args["h_i"]])) + [template_args["layout"]])

        elif (self.op == 'batch_normalization'):
            self.op = 'triNNity::layer::BatchNormalizationLayer'

            # Set up input buffers
            if (self.op not in self.magic_layers):
                if (self.input_buffer_name is None):
                    papa = self.node.get_only_parent()
                    self.input_buffer_name = papa.name
                    self.input_buffer = ''
            else:
                self.input_buffer_name = self.node.name + '_input'
                self.input_buffer = 'A' + ' * ' + self.input_buffer_name + ';'
                plain_decls += [(self.input_buffer_name, self.input_buffer, 'A', str(prod([template_args["c_i"], template_args["w_i"], template_args["h_i"]])))]

            # Set up mean buffer
            self.mean_buffer_name = self.node.name + '_mean'
            self.mean_buffer = 'W' + ' * ' + self.mean_buffer_name + ';'
            read_decls += [(self.mean_buffer_name, self.mean_buffer, 'W', "1")]

            # Set up offset buffer
            self.offset_buffer_name = self.node.name + '_offset'
            self.offset_buffer = 'W' + ' * ' + self.offset_buffer_name + ';'
            read_decls += [(self.offset_buffer_name, self.offset_buffer, 'W', "1")]

            # Set up scale buffer
            self.scale_buffer_name = self.node.name + '_scale'
            self.scale_buffer = 'W' + ' * ' + self.scale_buffer_name + ';'
            read_decls += [(self.scale_buffer_name, self.scale_buffer, 'W', "1")]

            # Set up variance buffer
            self.variance_buffer_name = self.node.name + '_variance'
            self.variance_buffer = 'W' + ' * ' + self.variance_buffer_name + ';'
            read_decls += [(self.variance_buffer_name, self.variance_buffer, 'W', "1")]

            args = ', '.join(['A'] + list(map(str, [template_args["c_i"], template_args["w_i"], template_args["h_i"]])) + [template_args["layout"]])

        elif (self.op == 'multiply'):
            self.op = 'triNNity::layer::EltwiseLayer'
            self.elt_op = 'triNNity::ELTWISE_MUL'

            # Set up input buffers
            self.input_buffers = []
            parents = self.node.get_all_parents()
            for n in parents:
              self.input_buffers.append(n.name)

            args = ', '.join(['A', self.elt_op] + list(map(str, [template_args["count"]])))

        elif (self.op == 'add'):
            self.op = 'triNNity::layer::EltwiseLayer'
            self.elt_op = 'triNNity::ELTWISE_ADD'

            # Set up input buffers
            self.input_buffers = []
            parents = self.node.get_all_parents()
            for n in parents:
              self.input_buffers.append(n.name)

            args = ', '.join(['A', self.elt_op] + list(map(str, [template_args["count"]])))

        elif (self.op == 'max'):
            self.op = 'triNNity::layer::EltwiseLayer'
            self.elt_op = 'triNNity::ELTWISE_MAX'

            # Set up input buffers
            self.input_buffers = []
            parents = self.node.get_all_parents()
            for n in parents:
              self.input_buffers.append(n.name)

            args = ', '.join(['A', self.elt_op] + list(map(str, [template_args["count"]])))

        elif (self.op == 'flatten'):
            self.op = 'triNNity::layer::FlattenLayer'

            # Set up input buffers
            self.input_buffers = []
            parents = self.node.get_all_parents()
            for n in parents:
              self.input_buffers.append(n.name)

            args = ', '.join(['A'] + list(map(str, [template_args["c_i"], template_args["w_i"], template_args["h_i"]])))

        else:
            if (self.op not in self.magic_layers):
                print_stderr('triNNity backend does not implement layer \'' + self.op + '\'')
            args = ''

        dynamic_args = []
        if self.input_buffer_name:
            if self.input_buffer_name == 'data':
                dynamic_args += ['this->input']
            else:
                dynamic_args += ['this->layers["' + self.input_buffer_name + '"]->output']

        if self.input_buffers: # Concat layer has multiple inputs
            for x in self.input_buffers:
              dynamic_args.append('this->layers["' + x + '"]->output')

        if self.weights_buffer_name:
            dynamic_args += ['this->parameters["' + self.weights_buffer_name + '"]']

        if self.bias_buffer_name:
            dynamic_args += ['this->parameters["' + self.bias_buffer_name + '"]']
        else:
            if self.orig_op in ['conv', 'fc']:
                dynamic_args += ['nullptr']

        if self.orig_op == 'lrn':
            dynamic_args += ['0', str(self.kwargs['size']), str(self.kwargs['alpha']), str(self.kwargs['beta'])]

        if self.orig_op == 'batch_normalization':
            if self.mean_buffer_name:
              dynamic_args += ['this->parameters["' + self.mean_buffer_name + '"]']
            else:
                dynamic_args += ['nullptr']

            if self.offset_buffer_name:
                dynamic_args += ['this->parameters["' + self.offset_buffer_name + '"]']
            else:
                dynamic_args += ['nullptr']

            if self.scale_buffer_name:
                dynamic_args += ['this->parameters["' + self.scale_buffer_name + '"]']
            else:
                dynamic_args += ['nullptr']

            if self.variance_buffer_name:
                dynamic_args += ['this->parameters["' + self.variance_buffer_name + '"]']
            else:
                dynamic_args += ['nullptr']

        if self.output_buffer_name:
            dynamic_args += ['this->activations["' + self.output_buffer_name + '"]']

        outputs = []
        if (self.orig_op not in self.magic_layers):
            outputs += ['this->layers["' + self.node.name + '"] = new ' + self.op + '<' + args + '>' + '(' + ', '.join(dynamic_args) + ');']

        return (read_decls, plain_decls, outputs)


class MaybeActivated(object):

    def __init__(self, node, default=True):
        self.inject_kwargs = {}
        if node.metadata.get('relu', False) != default:
            self.inject_kwargs['relu'] = not default
        else:
            self.inject_kwargs['relu'] = default

    def __call__(self, *args, **kwargs):
        kwargs.update(self.inject_kwargs)
        return TrinnityNode(*args, **kwargs)


class TrinnityMapper(IRNodeMapper):

    def get_kernel_params(self, node):
        kernel_params = node.layer.kernel_parameters
        return kernel_params

    def map_convolution(self, node):
        kernel_params = self.get_kernel_params(node)
        kwargs = {}
        kwargs["p_h"] = kernel_params.pad_h
        kwargs["p_w"] = kernel_params.pad_w
        kwargs["k_h"] = kernel_params.kernel_h
        kwargs["k_w"] = kernel_params.kernel_w
        kwargs["s_h"] = kernel_params.stride_h
        kwargs["s_w"] = kernel_params.stride_w
        kwargs["c_o"] = node.output_shape[1]
        kwargs["c_i"] = node.parents[0].output_shape[1]
        kwargs["h_i"] = node.parents[0].output_shape[2]
        kwargs["w_i"] = node.parents[0].output_shape[3]
        kwargs["h_o"] = int(math.ceil(kwargs["h_i"] / kwargs["s_h"]))
        kwargs["w_o"] = int(math.ceil(kwargs["w_i"] / kwargs["s_w"]))

        if kwargs["p_h"] == 0:
            kwargs["h_o"] = kwargs["h_o"] - (2 * math.floor(kwargs["k_h"] / 2))

        if kwargs["p_w"] == 0:
            kwargs["w_o"] = kwargs["w_o"] - (2 * math.floor(kwargs["k_w"] / 2))

        if kwargs["p_h"] == 0 and kwargs["p_w"] == 0:
            kwargs['pad'] = 'discard'
        else:
            kwargs['pad'] = 'implicit'

        group = node.parameters.group
        if group != 1:
            kwargs['group'] = group

        if node.parameters.bias_term:
            kwargs['biased'] = True
        else:
            kwargs['biased'] = False

        return MaybeActivated(node)('conv', **kwargs)

    def map_relu(self, node):
        kwargs = {}
        kwargs["c_i"] = node.parents[0].output_shape[1]
        kwargs["h_i"] = node.parents[0].output_shape[2]
        kwargs["w_i"] = node.parents[0].output_shape[3]
        kwargs["act"] = 'triNNity::ACTIVATION_RELU'

        return TrinnityNode('relu', **kwargs)

    def map_pooling(self, node):
        pool_type = node.parameters.pool
        kernel_params = self.get_kernel_params(node)
        kwargs = {}
        kwargs["k_h"] = kernel_params.kernel_h
        kwargs["k_w"] = kernel_params.kernel_w
        kwargs["s_h"] = kernel_params.stride_h
        kwargs["s_w"] = kernel_params.stride_w
        kwargs["c_o"] = node.output_shape[1]
        kwargs["c_i"] = node.parents[0].output_shape[1]
        kwargs["h_i"] = node.parents[0].output_shape[2]
        kwargs["w_i"] = node.parents[0].output_shape[3]
        kwargs["h_o"] = int(math.ceil(kwargs["h_i"] / kwargs["s_h"]))
        kwargs["w_o"] = int(math.ceil(kwargs["w_i"] / kwargs["s_w"]))

        if pool_type == 0:
            pool_op = 'max_pool'
        elif pool_type == 1:
            pool_op = 'avg_pool'
        else:
            raise CompilerError('Unsupported pooling type.')

        return TrinnityNode(pool_op, **kwargs)

    def map_inner_product(self, node):
        assert node.parameters.axis == 1
        assert node.parameters.bias_term == True
        kwargs = {}
        kwargs["c_i"] = node.parents[0].output_shape[1]
        kwargs["h_i"] = node.parents[0].output_shape[2]
        kwargs["w_i"] = node.parents[0].output_shape[3]
        kwargs["c_o"] = node.parameters.num_output

        if node.parameters.bias_term:
            kwargs['biased'] = True
        else:
            kwargs['biased'] = False

        return MaybeActivated(node)('fc', **kwargs)

    def map_softmax(self, node):
        kwargs = {}
        kwargs["c_i"] = node.parents[0].output_shape[1]

        return TrinnityNode('softmax', **kwargs)

    def map_lrn(self, node):
        params = node.parameters
        assert params.local_size % 2 == 1
        kwargs = {}
        kwargs["c_i"] = node.parents[0].output_shape[1]
        kwargs["h_i"] = node.parents[0].output_shape[2]
        kwargs["w_i"] = node.parents[0].output_shape[3]
        kwargs["alpha"] = params.alpha
        kwargs["beta"] = params.beta
        kwargs["size"] = params.local_size
        kwargs["layout"] = 'triNNity::layout::CHW'

        return TrinnityNode('lrn', **kwargs)

    def map_concat(self, node):
        axis = [node.parameters.axis]
        if axis != [1]:
            raise CompilerError('Found concat node with unsupported join axis: %s' % axis)

        kwargs = {}
        kwargs["c_i_0"] = node.parents[0].output_shape[1]
        kwargs["c_i_1"] = node.parents[1].output_shape[1]
        kwargs["h_i"] = node.parents[0].output_shape[2]
        kwargs["w_i"] = node.parents[0].output_shape[3]
        kwargs["layout"] = 'triNNity::layout::CHW'

        return TrinnityNode('concat', **kwargs)

    def map_dropout(self, node):
        kwargs = {}
        kwargs["ratio"] = node.parameters.dropout_ratio

        return TrinnityNode('dropout', **kwargs)

    def map_batch_norm(self, node):
        kwargs = {}
        kwargs["c_i"] = node.parents[0].output_shape[1]
        kwargs["h_i"] = node.parents[0].output_shape[2]
        kwargs["w_i"] = node.parents[0].output_shape[3]
        kwargs["layout"] = 'triNNity::layout::CHW'

        return MaybeActivated(node, default=False)('batch_normalization', **kwargs)

    def map_eltwise(self, node):
        operations = {0: 'multiply', 1: 'add', 2: 'max'}
        op_code = node.parameters.operation
        elt_count = 1
        for x in node.output_shape:
            elt_count *= x

        kwargs = {}
        kwargs["count"] = elt_count
        try:
            return TrinnityNode(operations[op_code], **kwargs)
        except KeyError:
            raise CompilerError('Unknown elementwise operation: {}'.format(op_code))

    def map_flatten(self, node):
        kwargs = {}
        kwargs["c_i"] = node.parents[0].output_shape[1]
        kwargs["h_i"] = node.parents[0].output_shape[2]
        kwargs["w_i"] = node.parents[0].output_shape[3]

        return TrinnityNode('flatten', **kwargs)

    def commit(self, chains):
        return chains


class TrinnityEmitter(object):

    def __init__(self, tab=None):
        self.tab = tab or ' ' * 2
        self.prefix = ''
        self.collected_rdeclarations = []
        self.collected_pdeclarations = []
        self.collected_code = []
        self.collected_layers = []

    def indent(self):
        self.prefix += self.tab

    def outdent(self):
        self.prefix = self.prefix[:-len(self.tab)]

    def statement(self, s):
        return self.prefix + s + '\n'

    def emit_imports(self, name):
        return self.statement('#include "'+name+'.h"')

    def emit_parents(self, chain):
        assert len(chain)
        sep = '\n' + self.prefix
        s = sep.join(["'%s'" % parent.name for parent in chain[0].node.parents])
        return self.statement(s)

    def emit_node(self, node):
        (rdecls, pdecls, code) = node.emit()
        self.collected_rdeclarations += rdecls
        self.collected_pdeclarations += pdecls
        self.collected_code += list(map(lambda x: self.statement(str(x)), code))
        self.collected_layers += ['this->layers["' + node.node.name + '"]->execute();']

    def emit(self, name, chains):
        i = self.emit_imports(name)
        i += '\n'

        rd = self.statement('ACTIVATION_TYPE * data;')

        for chain in chains:
            for node in chain:
                self.emit_node(node)

        c = self.collected_code

        e = list(map(lambda x: self.statement(x), self.collected_layers))

        return [i, c, e]


class TrinnityTransformer(object):

    def __init__(self, def_path, data_path, verbose=True, phase='test'):
        self.verbose = verbose
        self.phase = phase
        self.params = None
        self.sources = None
        self.rdeclarations = None
        self.pdeclarations = None
        self.input_data_shape = []
        self.output_node_name = None
        self.load(def_path, data_path, phase)

    def load(self, def_path, data_path, phase):
        # Build the graph
        graph = IRGraphBuilder(def_path, phase).build()

        if data_path is not None:
            # Load and associate learned parameters
            graph = DataInjector(def_path, data_path)(graph)

        # Transform the graph
        transformers = [
            # Fuse split batch normalization layers
            BatchNormScaleBiasFuser(),
            # Rename nodes
            # (Caffe's GoogLeNet implementation uses slashes)
            NodeRenamer(lambda node: node.name.replace('/', '_')),
            # Split concat operations into balanced binary trees
            ConcatTreeSplitter()
        ]
        self.graph = graph.transformed(transformers)

        topsorted_graph = self.graph.topologically_sorted()

        for x in topsorted_graph[0].output_shape:
            self.input_data_shape.append(x)

        self.labels = 1
        for x in topsorted_graph[-1].output_shape:
            self.labels *= x

        self.output_node_name = topsorted_graph[-1].name

        # Display the graph
        if self.verbose:
            print_stderr(self.graph)

    def transform_data(self):
        if self.params is None:
            transformers = [
                # Pre-process batch normalization data
                BatchNormPreprocessor(),
                # Convert parameters to dictionaries
                ParameterNamer(),
            ]
            self.graph = self.graph.transformed(transformers)
            self.params = {node.name: node.data for node in self.graph.topologically_sorted() if node.data}
        else:
            raise CompilerError('expected None for params, got: {}'.format(self.params))
        return self.params

    def transform_source(self):
        if self.sources is None:
            # Transform the graph
            transformers = [
                # Fuse split batch normalization layers
                BatchNormScaleBiasFuser(),
                # Rename nodes
                # (Caffe's GoogLeNet implementation uses slashes)
                NodeRenamer(lambda node: node.name.replace('/', '_')),
                # Split concat operations into balanced binary trees
                ConcatTreeSplitter(),
                # Fuse ReLUs
                # ReLUFuser(allowed_parent_types=[LayerKind.Convolution])
            ]
            self.graph = self.graph.transformed(transformers)

            mapper = TrinnityMapper()
            chains = mapper.map(self.graph)
            emitter = TrinnityEmitter()
            self.sources = emitter.emit(self.graph.name, chains)
            self.rdeclarations = emitter.collected_rdeclarations
            self.pdeclarations = emitter.collected_pdeclarations
        return self.sources
