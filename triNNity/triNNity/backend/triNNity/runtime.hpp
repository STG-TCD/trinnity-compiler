#ifndef TRINNITY_COMPILER_RUNTIME_TRINNITY
#define TRINNITY_COMPILER_RUNTIME_TRINNITY

#include <string>
#include <vector>
#include <map>

#include <triNNity/layer.h>

template <typename A, typename W>
class Model {

protected:
  std::vector<unsigned> input_dims;
  bool benchmarking_mode;
  std::string parameters_path;
  std::string output_path;
  std::string input_path;
  std::map<std::string, W*> parameters;
  std::map<std::string, triNNity::layer::Layer<A, A>*> layers;
  A *input, *output;

public:

  virtual void setup() = 0;
  virtual void infer() = 0;
  virtual void teardown() = 0;
  virtual void set_input(const A*) = 0;
  virtual A* get_output() = 0;
  virtual void load_input() = 0;
  virtual void save_output() = 0;

  double working_space() {
    double ws = 0.0;
    for (typename std::map<std::string, triNNity::layer::Layer<A, A>*>::iterator it = layers.begin(); it != layers.end(); ++it) {
      ws += it->second->working_space();
    }
    return ws;
  }

  std::vector<unsigned> input_dimensions() {
    return this->input_dimensions;
  }

  Model(std::string ppath, std::string opath, std::string ipath, std::vector<unsigned> idims, bool benchmode) {
    this->parameters_path = ppath;
    this->output_path = opath;
    this->input_path = ipath;
    this->benchmarking_mode = benchmode;
    this->input_dims = idims;
  }

  Model(std::string ppath, std::vector<unsigned> idims, bool benchmode) {
    this->parameters_path = ppath;
    this->output_path = "";
    this->input_path = "";
    this->benchmarking_mode = benchmode;
    this->input_dims = idims;
  }

};

#endif // TRINNITY_COMPILER_RUNTIME_TRINNITY
